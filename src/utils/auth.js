import Cookies from 'js-cookie'

const TokenKey = 'token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token, { secure: false, domain: process.env.DOMAIN, path: '/' })
}

export function setTokenExpire(token, date) {
  const date1 = new Date()
  const date2 = new Date(date)
  const diffDays = parseFloat((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24))
  return Cookies.set(TokenKey, token, { expires: diffDays, secure: false, domain: process.env.DOMAIN, path: '/' })
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
