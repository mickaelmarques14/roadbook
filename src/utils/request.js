import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import { getToken, setToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: process.env.BASE_API, // apibase_url
  timeout: 20000 // request timeout
})

// request interceptor
service.interceptors.request.use(config => {
  // Do something before request is sent
  if (store.getters.token) {
  // Let each request carry a token--['Authorization'] as a custom key. Please modify it according to the actual situation. 'Content-Type': 'multipart/form-data'
    config.headers['Content-Type'] = 'application/json'
    config.headers['Authorization'] = 'Bearer ' + getToken()
  }
  config.headers['Accept-language'] = store.getters.language || 'pt'
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone interceptor
service.interceptors.response.use(
  response => {
    if (response.headers.authorization) {
      const newtoken = response.headers.authorization.split(' ')
      store.commit('SET_TOKEN', newtoken[1])
      setToken(newtoken[1])
    }
    return response
  },
  // response => response,
  /**
   * The following comment indicates that the request status is indicated by a custom code in the response.
   * When the code returns the following, it indicates that there is a problem with the permissions, log out and return to the login page.
   * If you want to use the xmlhttprequest status code identification logic can be written in the following error
   */
  // response => {
  //   const res = response.data
  //   if (res.code !== 20000) {
  //     Message({
  //       message: res.message,
  //       type: 'error',
  //       duration: 5 * 1000
  //     })
  //     // 50008:Illegal token; 50012: Other clients logged in; 50014: Token expired;
  //     if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
  //       //  MessageBox
  //       // import { Message, MessageBox } from 'element-ui'
  //       MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'OK', {
  //         confirmButtonText: 'ok',
  //         cancelButtonText: this.$t('global.cancel'),
  //         type: 'warning'
  //       }).then(() => {
  //         store.dispatch('FedLogOut').then(() => {
  //           location.reload() // To re-instantiate the vue-router object to avoid bugs
  //         })
  //       })
  //     }
  //     return Promise.reject('error')
  //   } else {
  //     return response.data
  //   }
  // },
  error => {
    console.log(error.response)
    console.log('err', error) // for debug
    if (error.response && error.response.status === 403) {
      store.dispatch('FedLogOut').then(() => {
        location.reload() // To re-instantiate the vue-router object to avoid bugs
      })
    } else {
      Message({
        showClose: true,
        message: error.response.data.message, // error.message,
        type: 'error',
        duration: 5 * 1000
      })
    }
    return Promise.reject(error)
  })

export default service
