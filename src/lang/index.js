import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementPtLocale from 'element-ui/lib/locale/lang/pt'// element-ui lang
import enLocale from './en'
import ptLocale from './pt'

Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale
  },
  pt: {
    ...ptLocale,
    ...elementPtLocale
  }
}

const i18n = new VueI18n({
  // set locale
  // options: en or zh
  locale: Cookies.get('userlanguage') || 'pt',
  // set locale messages
  messages
})

export default i18n
