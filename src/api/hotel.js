import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/hotel',
    method: 'get',
    params: query
  })
}

export function fetchHotel(id) {
  return request({
    url: '/hotel/' + id,
    method: 'get'
  })
}

export function createHotel(data) {
  return request({
    url: '/hotel',
    method: 'post',
    data
  })
}

export function updateHotel(id, data) {
  return request({
    url: '/hotel/' + id,
    method: 'put',
    data
  })
}

export function deleteHotel(id) {
  return request({
    url: '/hotel/' + id,
    method: 'delete'
  })
}

export function searchHotel(query) {
  return request({
    url: '/hotel/search',
    method: 'get',
    params: query
  })
}
