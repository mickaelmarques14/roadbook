import request from '@/utils/request'

export function createStagedocumentcomment(data) {
  return request({
    url: '/stagedocumentcomment',
    method: 'post',
    data
  })
}

export function updateStagedocumentcomment(id, data) {
  return request({
    url: '/stagedocumentcomment/' + id,
    method: 'put',
    data
  })
}

export function deleteStagedocumentcomment(id) {
  return request({
    url: '/stagedocumentcomment/' + id,
    method: 'delete'
  })
}
