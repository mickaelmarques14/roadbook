import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/staff',
    method: 'get',
    params: query
  })
}

export function fetchStaffProduction(id, query) {
  return request({
    url: '/production/' + id + '/staff',
    method: 'get',
    params: query
  })
}

export function fetchStaff(id) {
  return request({
    url: '/staff/' + id,
    method: 'get'
  })
}

export function fetchStaffAll(id) {
  return request({
    url: '/staff/' + id + '/user',
    method: 'get'
  })
}

export function createStaff(data) {
  return request({
    url: '/staff',
    method: 'post',
    data
  })
}

export function updateStaff(id, data) {
  return request({
    url: '/staff/' + id,
    method: 'put',
    data
  })
}

export function deleteStaff(id) {
  return request({
    url: '/staff/' + id,
    method: 'delete'
  })
}

export function searchStaff(query) {
  return request({
    url: '/staff/search',
    method: 'get',
    params: query
  })
}
