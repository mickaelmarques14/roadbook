import request from '@/utils/request'

export function createStagecost(data) {
  return request({
    url: '/stagecost',
    method: 'post',
    data
  })
}

export function updateStagecost(id, data) {
  return request({
    url: '/stagecost/' + id,
    method: 'put',
    data
  })
}

export function deleteStagecost(id) {
  return request({
    url: '/stagecost/' + id,
    method: 'delete'
  })
}

export function fetchList(id, query) {
  return request({
    url: '/stage/' + id + '/cost',
    method: 'get',
    params: query
  })
}

export function fetchEventStageCosts(id) {
  return request({
    url: '/event/' + id + '/stagescost',
    method: 'get'
  })
}
