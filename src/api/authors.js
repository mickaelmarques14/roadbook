import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/authors',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/authors/' + id,
    method: 'get',
    params: { }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/article/create',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/authors/' + data.id,
    method: 'put',
    data
  })
}
