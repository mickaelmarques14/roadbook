import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/restaurant',
    method: 'get',
    params: query
  })
}

export function fetchRestaurant(id) {
  return request({
    url: '/restaurant/' + id,
    method: 'get'
  })
}

export function createRestaurant(data) {
  return request({
    url: '/restaurant',
    method: 'post',
    data
  })
}

export function updateRestaurant(id, data) {
  return request({
    url: '/restaurant/' + id,
    method: 'put',
    data
  })
}

export function deleteRestaurant(id) {
  return request({
    url: '/restaurant/' + id,
    method: 'delete'
  })
}

export function searchRestaurant(query) {
  return request({
    url: '/restaurant/search',
    method: 'get',
    params: query
  })
}
