import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/stagestafftechnician',
    method: 'get',
    params: query
  })
}

export function fetchStagestafftechnician(id) {
  return request({
    url: '/stagestafftechnician/' + id,
    method: 'get'
  })
}

export function createStagestafftechnician(data) {
  return request({
    url: '/stagestafftechnician',
    method: 'post',
    data
  })
}

export function updateStagestafftechnician(id, data) {
  return request({
    url: '/stagestafftechnician/' + id,
    method: 'put',
    data
  })
}

export function deleteStagestafftechnician(id) {
  return request({
    url: '/stagestafftechnician/' + id,
    method: 'delete'
  })
}

export function searchStagestafftechnician(query) {
  return request({
    url: '/stagestafftechnician/search',
    method: 'get',
    params: query
  })
}

export function postStagestafftechnician(id, data) {
  return request({
    url: '/stage/' + id + '/stafftechnician',
    method: 'post',
    data
  })
}
