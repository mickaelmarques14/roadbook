import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/schedule',
    method: 'get',
    params: query
  })
}

export function fetchSchedule(id) {
  return request({
    url: '/schedule/' + id,
    method: 'get'
  })
}

export function createSchedule(data) {
  return request({
    url: '/schedule',
    method: 'post',
    data
  })
}

export function updateSchedule(id, data) {
  return request({
    url: '/schedule/' + id,
    method: 'put',
    data
  })
}

export function deleteSchedule(id) {
  return request({
    url: '/schedule/' + id,
    method: 'delete'
  })
}

export function searchSchedule(query) {
  return request({
    url: '/schedule/search',
    method: 'get',
    params: query
  })
}

export function fetchStageSchedule(id, query) {
  return request({
    url: '/stage/' + id + '/scheduledays',
    method: 'get',
    params: query
  })
}

export function fetchAllList(query) {
  return request({
    url: '/schedule/all',
    method: 'get',
    params: query
  })
}
