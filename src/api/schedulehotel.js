import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/schedulehotel',
    method: 'get',
    params: query
  })
}

export function fetchSchedulehotel(id) {
  return request({
    url: '/schedulehotel/' + id,
    method: 'get'
  })
}

export function createSchedulehotel(data) {
  return request({
    url: '/schedulehotel',
    method: 'post',
    data
  })
}

export function updateSchedulehotel(id, data) {
  return request({
    url: '/schedulehotel/' + id,
    method: 'put',
    data
  })
}

export function deleteSchedulehotel(id) {
  return request({
    url: '/schedulehotel/' + id,
    method: 'delete'
  })
}

export function searchSchedulehotel(query) {
  return request({
    url: '/schedulehotel/search',
    method: 'get',
    params: query
  })
}

export function fetchStageSchedulehotel(id, query) {
  return request({
    url: '/stage/' + id + '/schedulehoteldays',
    method: 'get',
    params: query
  })
}
