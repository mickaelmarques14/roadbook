import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/scheduleperformance',
    method: 'get',
    params: query
  })
}

export function fetchScheduleperformance(id) {
  return request({
    url: '/scheduleperformance/' + id,
    method: 'get'
  })
}

export function createScheduleperformance(data) {
  return request({
    url: '/scheduleperformance',
    method: 'post',
    data
  })
}

export function updateScheduleperformance(id, data) {
  return request({
    url: '/scheduleperformance/' + id,
    method: 'put',
    data
  })
}

export function deleteScheduleperformance(id) {
  return request({
    url: '/scheduleperformance/' + id,
    method: 'delete'
  })
}

export function searchScheduleperformance(query) {
  return request({
    url: '/scheduleperformance/search',
    method: 'get',
    params: query
  })
}

export function fetchStageScheduleperformance(id, query) {
  return request({
    url: '/stage/' + id + '/scheduleperformancedays',
    method: 'get',
    params: query
  })
}
