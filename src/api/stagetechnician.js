import request from '@/utils/request'

export function createStageTechnician(data) {
  return request({
    url: '/stagetechnician',
    method: 'post',
    data
  })
}

export function updateStageTechnician(id, data) {
  return request({
    url: '/stagetechnician/' + id,
    method: 'put',
    data
  })
}

export function deleteStageTechnician(id) {
  return request({
    url: '/stagetechnician/' + id,
    method: 'delete'
  })
}

