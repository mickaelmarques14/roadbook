import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/schedulesetup',
    method: 'get',
    params: query
  })
}

export function fetchSchedulesetup(id) {
  return request({
    url: '/schedulesetup/' + id,
    method: 'get'
  })
}

export function createSchedulesetup(data) {
  return request({
    url: '/schedulesetup',
    method: 'post',
    data
  })
}

export function updateSchedulesetup(id, data) {
  return request({
    url: '/schedulesetup/' + id,
    method: 'put',
    data
  })
}

export function deleteSchedulesetup(id) {
  return request({
    url: '/schedulesetup/' + id,
    method: 'delete'
  })
}

export function searchSchedulesetup(query) {
  return request({
    url: '/schedulesetup/search',
    method: 'get',
    params: query
  })
}

export function fetchStageSchedulesetup(id, query) {
  return request({
    url: '/stage/' + id + '/schedulesetupdays',
    method: 'get',
    params: query
  })
}
