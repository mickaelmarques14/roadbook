import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/artist',
    method: 'get',
    params: query
  })
}

export function fetchArtist(id) {
  return request({
    url: '/artist/' + id,
    method: 'get'
  })
}

export function createArtist(data) {
  return request({
    url: '/artist',
    method: 'post',
    data
  })
}

export function updateArtist(id, data) {
  return request({
    url: '/artist/' + id,
    method: 'put',
    data
  })
}

export function deleteArtist(id) {
  return request({
    url: '/artist/' + id,
    method: 'delete'
  })
}

export function searchArtist(query) {
  return request({
    url: '/artist/search',
    method: 'get',
    params: query
  })
}
