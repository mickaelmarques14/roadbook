export default [
  { key: 'superadmin',
    value: 'Super Admin' },
  { key: 'admin',
    value: 'Admin' },
  { key: 'adminproduction',
    value: 'Production Manager' },
  { key: 'production',
    value: 'Production Staff' },
  { key: 'technician',
    value: 'Technician' },
  { key: 'promoter',
    value: 'Promoter' },
  { key: 'artist',
    value: 'Artist' },
  { key: 'manager',
    value: 'Manager' },
  { key: 'user',
    value: 'User' }
]

export const productionRoles = [
  { key: 'adminproduction',
    value: 'Production Manager' },
  { key: 'production',
    value: 'Production Staff' }
]
