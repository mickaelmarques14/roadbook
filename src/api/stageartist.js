import request from '@/utils/request'

export function createStageArtist(data) {
  return request({
    url: '/stageartist',
    method: 'post',
    data
  })
}

export function updateStageArtist(id, data) {
  return request({
    url: '/stageartist/' + id,
    method: 'put',
    data
  })
}

export function deleteStageArtist(id) {
  return request({
    url: '/stageartist/' + id,
    method: 'delete'
  })
}

