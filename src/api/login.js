import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    email: username,
    password
  }
  return request({
    url: '/auth',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'delete'
  })
}

export function getUserInfo() {
  return request({
    url: '/user',
    method: 'get'
  })
}

export function refreshToken() {
  return request({
    url: '/authorizations/current',
    method: 'put'
  })
}

export function recover(data) {
  return request({
    url: '/recover',
    method: 'post',
    data
  })
}

export function updatepassword(data) {
  return request({
    url: '/resetpassword',
    method: 'put',
    data
  })
}
