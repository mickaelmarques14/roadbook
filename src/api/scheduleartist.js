import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/scheduleartist',
    method: 'get',
    params: query
  })
}

export function fetchScheduleArtist(id) {
  return request({
    url: '/scheduleartist/' + id,
    method: 'get'
  })
}

export function createScheduleArtist(data) {
  return request({
    url: '/scheduleartist',
    method: 'post',
    data
  })
}

export function updateScheduleArtist(id, data) {
  return request({
    url: '/scheduleartist/' + id,
    method: 'put',
    data
  })
}

export function deleteScheduleArtist(id) {
  return request({
    url: '/scheduleartist/' + id,
    method: 'delete'
  })
}

export function searchScheduleArtist(query) {
  return request({
    url: '/scheduleartist/search',
    method: 'get',
    params: query
  })
}

export function fetchStageScheduleArtist(stage, artist) {
  return request({
    url: '/scheduleartist/stage/' + stage + '/artist/' + artist,
    method: 'get'
  })
}

export function fetchAllList(query) {
  return request({
    url: '/scheduleartist/all',
    method: 'get',
    params: query
  })
}

export function fetchScheduleStageArtist(id) {
  return request({
    url: '/schedulestageartist/' + id,
    method: 'get'
  })
}

