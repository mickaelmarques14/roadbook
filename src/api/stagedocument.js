import request from '@/utils/request'

export function uploadFile(file) {
  const formData = new FormData()
  formData.append('file', file)
  return request({
    url: '/stagedocumentupload',
    method: 'post',
    // config: { headers: { 'Content-Type': 'multipart/form-data' }},
    data: formData
  })
}

export function createStagedocument(data) {
  return request({
    url: '/stagedocument',
    method: 'post',
    data
  })
}

export function updateStagedocument(id, data) {
  return request({
    url: '/stagedocument/' + id,
    method: 'put',
    data
  })
}

export function deleteStagedocument(id) {
  return request({
    url: '/stagedocument/' + id,
    method: 'delete'
  })
}

export function postStagedocument(id, data) {
  return request({
    url: '/stage/' + id + '/document',
    method: 'post',
    data
  })
}
