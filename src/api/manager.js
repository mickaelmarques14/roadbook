import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/manager',
    method: 'get',
    params: query
  })
}

export function fetchManager(id) {
  return request({
    url: '/manager/' + id,
    method: 'get'
  })
}

export function createManager(data) {
  return request({
    url: '/manager',
    method: 'post',
    data
  })
}

export function updateManager(id, data) {
  return request({
    url: '/manager/' + id,
    method: 'put',
    data
  })
}

export function deleteManager(id) {
  return request({
    url: '/manager/' + id,
    method: 'delete'
  })
}

export function searchManager(query) {
  return request({
    url: '/manager/search',
    method: 'get',
    params: query
  })
}
