import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/stafftechnician',
    method: 'get',
    params: query
  })
}

export function fetchTechnicianStaff(id, query) {
  return request({
    url: '/technician/' + id + '/stafftechnician',
    method: 'get',
    params: query
  })
}

export function fetchTechnicianstaff(id) {
  return request({
    url: '/stafftechnician/' + id,
    method: 'get'
  })
}

export function fetchTechnicianstaffAll(id) {
  return request({
    url: '/stafftechnician/' + id + '/user',
    method: 'get'
  })
}

export function createTechnicianstaff(data) {
  return request({
    url: '/stafftechnician',
    method: 'post',
    data
  })
}

export function updateTechnicianstaff(id, data) {
  return request({
    url: '/stafftechnician/' + id,
    method: 'put',
    data
  })
}

export function deleteTechnicianstaff(id) {
  return request({
    url: '/stafftechnician/' + id,
    method: 'delete'
  })
}

export function searchTechnicianstaff(query) {
  return request({
    url: '/stafftechnician/search',
    method: 'get',
    params: query
  })
}
