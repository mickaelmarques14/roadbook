import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/scheduletechnician',
    method: 'get',
    params: query
  })
}

export function fetchScheduleTechnician(id) {
  return request({
    url: '/scheduletechnician/' + id,
    method: 'get'
  })
}

export function createScheduleTechnician(data) {
  return request({
    url: '/scheduletechnician',
    method: 'post',
    data
  })
}

export function updateScheduleTechnician(id, data) {
  return request({
    url: '/scheduletechnician/' + id,
    method: 'put',
    data
  })
}

export function deleteScheduleTechnician(id) {
  return request({
    url: '/scheduletechnician/' + id,
    method: 'delete'
  })
}

export function searchScheduleTechnician(query) {
  return request({
    url: '/scheduletechnician/search',
    method: 'get',
    params: query
  })
}

export function fetchStageScheduleTechnician(stage, technician) {
  return request({
    url: '/scheduletechnician/stage/' + stage + '/technician/' + technician,
    method: 'get'
  })
}

export function fetchAllList(query) {
  return request({
    url: '/scheduletechnician/all',
    method: 'get',
    params: query
  })
}

export function fetchScheduleStageTechnician(id) {
  return request({
    url: '/schedulestagetechnician/' + id,
    method: 'get'
  })
}
