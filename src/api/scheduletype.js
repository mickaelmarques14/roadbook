export default [
  { key: '1', display_name: 'Performance', color: '#fb6e5e', textcolor: '#000000' },
  { key: '2', display_name: 'Sound Check', color: '#ff9d52', textcolor: '#000000' },
  { key: '3', display_name: 'Hotel', color: '#a06bff', textcolor: '#000000' },
  { key: '4', display_name: 'Restaurant', color: '#abff57', textcolor: '#000000' },
  { key: '5', display_name: 'Other', color: '#4ec5ff', textcolor: '#000000' }
]

export const typeOptionsTechnician = [
  { key: '3', display_name: 'Hotel', color: '#a06bff', textcolor: '#000000' },
  { key: '4', display_name: 'Restaurant', color: '#abff57', textcolor: '#000000' },
  { key: '5', display_name: 'Other', color: '#4ec5ff', textcolor: '#000000' }
]
