import request from '@/utils/request'

export const statusList = [{
  key: 0,
  value: 'Invisible',
  valuept: 'Não Visível' },
{ key: 1,
  value: 'Visible',
  valuept: 'Visível' },
{ key: 2,
  value: 'Completed',
  valuept: 'Finalizado' }]

export function fetchList(query) {
  return request({
    url: '/event',
    method: 'get',
    params: query
  })
}

export function fetchUpcomingList(query) {
  return request({
    url: '/event/upcoming',
    method: 'get',
    params: query
  })
}

export function fetchEvent(id) {
  return request({
    url: '/event/' + id,
    method: 'get'
  })
}

export function fetchEventStages(id) {
  return request({
    url: '/eventstages/' + id,
    method: 'get'
  })
}

export function createEvent(data) {
  return request({
    url: '/event',
    method: 'post',
    data
  })
}

export function updateEvent(id, data) {
  return request({
    url: '/event/' + id,
    method: 'put',
    data
  })
}

export function deleteEvent(id) {
  return request({
    url: '/event/' + id,
    method: 'delete'
  })
}

export function searchEvent(query) {
  return request({
    url: '/event/search',
    method: 'get',
    params: query
  })
}

export function uploadImage(file) {
  const formData = new FormData()
  formData.append('file', file)
  return request({
    url: '/eventupload',
    method: 'post',
    // config: { headers: { 'Content-Type': 'multipart/form-data' }},
    data: formData
  })
}

export function generateRoadbook(id) {
  return request({
    url: '/event/roadbook/' + id,
    method: 'get'
  })
}
