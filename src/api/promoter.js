import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/promoter',
    method: 'get',
    params: query
  })
}

export function fetchPromoter(id) {
  return request({
    url: '/promoter/' + id,
    method: 'get'
  })
}

export function fetchPromoterAll(id) {
  return request({
    url: '/promoter/' + id + '/user',
    method: 'get'
  })
}

export function createPromoter(data) {
  return request({
    url: '/promoter',
    method: 'post',
    data
  })
}

export function updatePromoter(id, data) {
  return request({
    url: '/promoter/' + id,
    method: 'put',
    data
  })
}

export function deletePromoter(id) {
  return request({
    url: '/promoter/' + id,
    method: 'delete'
  })
}

export function searchPromoter(query) {
  return request({
    url: '/promoter/search',
    method: 'get',
    params: query
  })
}
