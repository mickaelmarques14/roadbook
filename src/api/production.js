import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/production',
    method: 'get',
    params: query
  })
}

export function fetchProduction(id) {
  return request({
    url: '/production/' + id,
    method: 'get'
  })
}

export function createProduction(data) {
  return request({
    url: '/production',
    method: 'post',
    data
  })
}

export function updateProduction(id, data) {
  return request({
    url: '/production/' + id,
    method: 'put',
    data
  })
}

export function deleteProduction(id) {
  return request({
    url: '/production/' + id,
    method: 'delete'
  })
}

export function searchProduction(query) {
  return request({
    url: '/production/search',
    method: 'get',
    params: query
  })
}
