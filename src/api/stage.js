import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/stage',
    method: 'get',
    params: query
  })
}

export function fetchStage(id) {
  return request({
    url: '/stage/' + id,
    method: 'get'
  })
}

export function createStage(data) {
  return request({
    url: '/stage',
    method: 'post',
    data
  })
}

export function updateStage(id, data) {
  return request({
    url: '/stage/' + id,
    method: 'put',
    data
  })
}

export function deleteStage(id) {
  return request({
    url: '/stage/' + id,
    method: 'delete'
  })
}

export function searchStage(query) {
  return request({
    url: '/stage/search',
    method: 'get',
    params: query
  })
}

export function fetchStageEvent(id) {
  return request({
    url: '/event/' + id + '/stage',
    method: 'get'
  })
}

export function listStageStafftechnician(id) {
  return request({
    url: '/stage/' + id + '/stafftechnician',
    method: 'get'
  })
}

export function listStagedocument(id) {
  return request({
    url: '/stage/' + id + '/document',
    method: 'get'
  })
}

export function fetchStageInformation(id) {
  return request({
    url: '/stage/' + id + '/information',
    method: 'get'
  })
}

export function generateRoadbook(id) {
  return request({
    url: '/stage/roadbook/' + id,
    method: 'get'
  })
}

export function fecthStageArtistTechnician(id) {
  return request({
    url: '/stage/' + id + '/artisttechnician/',
    method: 'get'
  })
}
