export default [
  { key: 'pending',
    value: 'Pending',
    class: 'warning' },
  { key: 'valid',
    value: 'Valid',
    class: 'success' },
  { key: 'invalid',
    value: 'Invalid',
    class: 'danger' }
]

export const Message = [
  { key: 'valid',
    value: 'Valid' },
  { key: 'invalid',
    value: 'Invalid' }
]

export const StatusObject = {
  pending: { key: 'pending',
    type: 'warning',
    value: 'Pending' },
  valid: { key: 'valid',
    type: 'success',
    value: 'Valid' },
  invalid: { key: 'invalid',
    type: 'danger',
    value: 'Invalid' }
}
