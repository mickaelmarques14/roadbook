import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/technician',
    method: 'get',
    params: query
  })
}

export function fetchTechnician(id) {
  return request({
    url: '/technician/' + id,
    method: 'get'
  })
}

export function createTechnician(data) {
  return request({
    url: '/technician',
    method: 'post',
    data
  })
}

export function updateTechnician(id, data) {
  return request({
    url: '/technician/' + id,
    method: 'put',
    data
  })
}

export function deleteTechnician(id) {
  return request({
    url: '/technician/' + id,
    method: 'delete'
  })
}

export function searchTechnician(query) {
  return request({
    url: '/technician/search',
    method: 'get',
    params: query
  })
}
