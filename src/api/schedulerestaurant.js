import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/schedulerestaurant',
    method: 'get',
    params: query
  })
}

export function fetchSchedulerestaurant(id) {
  return request({
    url: '/schedulerestaurant/' + id,
    method: 'get'
  })
}

export function createSchedulerestaurant(data) {
  return request({
    url: '/schedulerestaurant',
    method: 'post',
    data
  })
}

export function updateSchedulerestaurant(id, data) {
  return request({
    url: '/schedulerestaurant/' + id,
    method: 'put',
    data
  })
}

export function deleteSchedulerestaurant(id) {
  return request({
    url: '/schedulerestaurant/' + id,
    method: 'delete'
  })
}

export function searchSchedulerestaurant(query) {
  return request({
    url: '/schedulerestaurant/search',
    method: 'get',
    params: query
  })
}

export function fetchStageSchedulerestaurant(id, query) {
  return request({
    url: '/stage/' + id + '/schedulerestaurantdays',
    method: 'get',
    params: query
  })
}
