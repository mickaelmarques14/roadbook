import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'

import SimplifyLayout from '@/views/layout/SimplifyLayout'

/** note: submenu only apppear when children.length>=1
*   detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
**/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
**/

export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/recover', component: () => import('@/views/recover/index'), hidden: true },
  { path: '/recover/:key', component: () => import('@/views/recover/restore'), hidden: true },
  { path: '/authredirect', component: () => import('@/views/login/authredirect'), hidden: true },
  { path: '/404', component: () => import('@/views/errorPage/404'), hidden: true },
  { path: '/401', component: () => import('@/views/errorPage/401'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'dashboard',
      meta: { title: 'dashboard', icon: 'dashboard', noCache: true }
    }]
  }
]

export const simplifyconstantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/recover', component: () => import('@/views/recover/index'), hidden: true },
  { path: '/recover/:key', component: () => import('@/views/recover/restore'), hidden: true },
  { path: '/authredirect', component: () => import('@/views/login/authredirect'), hidden: true },
  { path: '/404', component: () => import('@/views/errorPage/404'), hidden: true },
  { path: '/401', component: () => import('@/views/errorPage/401'), hidden: true },
  {
    path: '',
    component: SimplifyLayout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'dashboard',
      meta: { title: 'dashboard', icon: 'dashboard', noCache: true }
    }]
  }
]

export default new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: simplifyconstantRouterMap
})

export const asyncRouterMap = [
  {
    path: '/dashboard',
    component: Layout,
    redirect: '/dashboard/start',
    children: [
      {
        path: 'start',
        component: () => import('@/views/dashboard/admin/custom'),
        name: 'start',
        meta: { title: 'Start', tagtitle: 'Start', icon: 'qq' }
      }
    ]
  },
  {
    path: '/promoter',
    component: Layout,
    redirect: '/promoter/list',
    name: 'promoter',
    meta: {
      title: 'Promoter',
      icon: 'promoter'
    },
    children: [
      { path: 'create', component: () => import('@/views/promoter/create'), name: 'createPromoter', meta: { title: 'Create', tagtitle: 'CreatePromoter', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/promoter/edit'), name: 'editPromoter', meta: { title: 'Edit', tagtitle: 'EditPromoter', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/promoter/view'), name: 'viewPromoter', meta: { title: 'View', tagtitle: 'ViewPromoter', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/promoter/list'), name: 'listPromoter', meta: { title: 'List', tagtitle: 'ListPromoter', icon: 'list' }}
    ]
  },
  {
    path: '/production',
    component: Layout,
    redirect: '/production/list',
    name: 'production',
    meta: {
      title: 'Production',
      icon: 'production'
    },
    children: [
      { path: 'create', component: () => import('@/views/production/create'), name: 'createProduction', meta: { title: 'Create', tagtitle: 'CreateProduction', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/production/edit'), name: 'editProduction', meta: { title: 'Edit', tagtitle: 'EditProduction', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/production/view'), name: 'viewProduction', meta: { title: 'View', tagtitle: 'ViewProduction', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/production/list'), name: 'listProduction', meta: { title: 'List', tagtitle: 'ListProduction', icon: 'list' }},
      { path: ':id(\\d+)/staff', component: () => import('@/views/production/staff'), name: 'staffProduction', meta: { title: 'List', tagtitle: 'ListStaff', icon: 'list' }, hidden: true }
    ]
  },
  {
    path: '/staff',
    component: Layout,
    redirect: '/staff/list',
    name: 'staff',
    meta: {
      title: 'Staff',
      icon: 'staff'
    },
    children: [
      { path: 'create', component: () => import('@/views/staff/create'), name: 'createStaff', meta: { title: 'Create', tagtitle: 'CreateStaff', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/staff/edit'), name: 'editStaff', meta: { title: 'Edit', tagtitle: 'EditStaff', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/staff/view'), name: 'viewStaff', meta: { title: 'View', tagtitle: 'ViewStaff', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/staff/list'), name: 'listStaff', meta: { title: 'List', tagtitle: 'ListStaff', icon: 'list' }}
    ]
  },
  {
    path: '/event',
    component: Layout,
    redirect: '/event/list',
    name: 'event',
    meta: {
      title: 'Event',
      icon: 'event'
    },
    children: [
      { path: 'create', component: () => import('@/views/event/create'), name: 'createEvent', meta: { title: 'Create', tagtitle: 'CreateEvent', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/event/edit'), name: 'editEvent', meta: { title: 'Edit', tagtitle: 'EditEvent', noCache: true }, hidden: true },
      { path: 'cost/:id(\\d+)', component: () => import('@/views/event/cost'), name: 'costStage', meta: { title: 'Cost', tagtitle: 'CostStage', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/event/view'), name: 'viewEvent', meta: { title: 'View', tagtitle: 'ViewEvent', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/event/list'), name: 'listEvent', meta: { title: 'List', tagtitle: 'ListEvent', icon: 'list' }}
    ]
  },
  {
    path: '/stage',
    component: Layout,
    redirect: '/stage/list',
    name: 'stage',
    meta: {
      title: 'Stage',
      icon: 'stage'
    },
    children: [
      { path: 'create', component: () => import('@/views/stage/create'), name: 'createStage', meta: { title: 'Create', tagtitle: 'CreateStage', icon: 'edit' }},
      { path: 'extend/:id(\\d+)', component: () => import('@/views/stage/extend'), name: 'extendStage', meta: { title: 'More', tagtitle: 'Information Stage', noCache: true }, hidden: true },
      { path: 'edit/:id(\\d+)', component: () => import('@/views/stage/edit'), name: 'editStage', meta: { title: 'Edit', tagtitle: 'EditStage', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/stage/view'), name: 'viewStage', meta: { title: 'View', tagtitle: 'ViewStage', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/stage/list'), name: 'listStage', meta: { title: 'List', tagtitle: 'ListStage', icon: 'list' }}
    ]
  },
  {
    path: '/schedule',
    component: Layout,
    redirect: '/schedule/list',
    name: 'schedule',
    meta: {
      title: 'Schedule',
      icon: 'schedule'
    },
    children: [
      { path: 'create', component: () => import('@/views/schedule/create'), name: 'createSchedule', meta: { title: 'Create', tagtitle: 'CreateSchedule', icon: 'edit' }},
      { path: ':type(\\d+)/edit/:id(\\d+)', component: () => import('@/views/schedule/edit'), name: 'editSchedule', meta: { title: 'Edit', tagtitle: 'EditSchedule', noCache: true }, hidden: true },
      { path: ':type(\\d+)/view/:id(\\d+)', component: () => import('@/views/schedule/view'), name: 'viewSchedule', meta: { title: 'View', tagtitle: 'ViewSchedule', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/schedule/list'), name: 'listSchedule', meta: { title: 'List', tagtitle: 'ListSchedule', icon: 'list' }}
    ]
  },
  {
    path: '/artist',
    component: Layout,
    redirect: '/artist/list',
    name: 'artist',
    meta: {
      title: 'Artist',
      icon: 'artist'
    },
    children: [
      { path: 'create', component: () => import('@/views/artist/create'), name: 'createArtist', meta: { title: 'Create', tagtitle: 'CreateArtist', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/artist/edit'), name: 'editArtist', meta: { title: 'Edit', tagtitle: 'EditArtist', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/artist/view'), name: 'viewArtist', meta: { title: 'View', tagtitle: 'ViewArtist', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/artist/list'), name: 'listArtist', meta: { title: 'List', tagtitle: 'ListArtist', icon: 'list' }}
    ]
  },
  {
    path: '/manager',
    component: Layout,
    redirect: '/manager/list',
    name: 'manager',
    meta: {
      title: 'Manager',
      icon: 'manager'
    },
    children: [
      { path: 'create', component: () => import('@/views/manager/create'), name: 'createManager', meta: { title: 'Create', tagtitle: 'CreateManager', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/manager/edit'), name: 'editManager', meta: { title: 'Edit', tagtitle: 'EditManager', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/manager/view'), name: 'viewManager', meta: { title: 'View', tagtitle: 'ViewManager', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/manager/list'), name: 'listManager', meta: { title: 'List', tagtitle: 'ListManager', icon: 'list' }}
    ]
  },
  {
    path: '/technician',
    component: Layout,
    redirect: '/technician/list',
    name: 'technician',
    meta: {
      title: 'Technician',
      icon: 'technician'
    },
    children: [
      { path: 'create', component: () => import('@/views/technician/create'), name: 'createTechnician', meta: { title: 'Create', tagtitle: 'CreateTechnician', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/technician/edit'), name: 'editTechnician', meta: { title: 'Edit', tagtitle: 'EditTechnician', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/technician/view'), name: 'viewTechnician', meta: { title: 'View', tagtitle: 'ViewTechnician', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/technician/list'), name: 'listTechnician', meta: { title: 'List', tagtitle: 'ListTechnician', icon: 'list' }},
      { path: ':id(\\d+)/technicianstaff', component: () => import('@/views/technician/staff'), name: 'staffTechnician', meta: { title: 'List', tagtitle: 'ListTechnicianstaff', icon: 'list' }, hidden: true }
    ]
  },
  {
    path: '/technicianstaff',
    component: Layout,
    redirect: '/technicianstaff/list',
    name: 'technicianstaff',
    meta: {
      title: 'Technician Staff',
      icon: 'staff'
    },
    children: [
      { path: 'create', component: () => import('@/views/technicianstaff/create'), name: 'createTechnicianstaff', meta: { title: 'Create', tagtitle: 'CreateTechnicianstaff', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/technicianstaff/edit'), name: 'editTechnicianstaff', meta: { title: 'Edit', tagtitle: 'EditTechnicianstaff', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/technicianstaff/view'), name: 'viewTechnicianstaff', meta: { title: 'View', tagtitle: 'ViewTechnicianstaff', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/technicianstaff/list'), name: 'listTechnicianstaff', meta: { title: 'List', tagtitle: 'ListTechnicianstaff', icon: 'list' }}
    ]
  },
  {
    path: '/hotel',
    component: Layout,
    redirect: '/hotel/list',
    name: 'hotel',
    meta: {
      title: 'Hotel',
      icon: 'hotel'
    },
    children: [
      { path: 'create', component: () => import('@/views/hotel/create'), name: 'createHotel', meta: { title: 'Create', tagtitle: 'CreateHotel', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/hotel/edit'), name: 'editHotel', meta: { title: 'Edit', tagtitle: 'EditHotel', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/hotel/view'), name: 'viewHotel', meta: { title: 'View', tagtitle: 'ViewHotel', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/hotel/list'), name: 'listHotel', meta: { title: 'List', tagtitle: 'ListHotel', icon: 'list' }}
    ]
  },
  {
    path: '/restaurant',
    component: Layout,
    redirect: '/restaurant/list',
    name: 'restaurant',
    meta: {
      title: 'Restaurant',
      icon: 'restaurant'
    },
    children: [
      { path: 'create', component: () => import('@/views/restaurant/create'), name: 'createRestaurant', meta: { title: 'Create', tagtitle: 'CreateRestaurant', icon: 'edit' }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/restaurant/edit'), name: 'editRestaurant', meta: { title: 'Edit', tagtitle: 'EditRestaurant', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/restaurant/view'), name: 'viewRestaurant', meta: { title: 'View', tagtitle: 'ViewRestaurant', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/restaurant/list'), name: 'listRestaurant', meta: { title: 'List', tagtitle: 'ListRestaurant', icon: 'list' }}
    ]
  },
  {
    path: '/user',
    component: Layout,
    redirect: '/user/list',
    name: 'user',
    meta: {
      title: 'User',
      icon: 'user'
    },
    children: [
      { path: 'create', component: () => import('@/views/user/create'), name: 'createUser', meta: { title: 'Create', tagtitle: 'CreateUser', icon: 'edit', roles: ['superadmin'] }},
      { path: 'edit/:id(\\d+)', component: () => import('@/views/user/edit'), name: 'editUser', meta: { title: 'Edit', tagtitle: 'EditUser', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/user/view'), name: 'viewUser', meta: { title: 'View', tagtitle: 'ViewUser', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/user/list'), name: 'userList', meta: { title: 'List', icon: 'list', tagtitle: 'ListUser', noCache: true }}
    ]
  },
  {
    path: '/permission',
    component: Layout,
    redirect: 'noredirect',
    alwaysShow: true, // will always show the root menu
    meta: {
      title: 'permission',
      icon: 'lock',
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [{
      path: 'page',
      component: () => import('@/views/permission/page'),
      name: 'pagePermission',
      meta: {
        title: 'pagePermission',
        roles: ['admin'] // or you can only set roles in sub nav
      }
    }, {
      path: 'directive',
      component: () => import('@/views/permission/directive'),
      name: 'directivePermission',
      meta: {
        title: 'directivePermission'
        // if do not set roles, means: this page does not require permission
      }
    }]
  },
  {
    path: '/icon',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/svg-icons/index'),
      name: 'icons',
      meta: { title: 'icons', icon: 'icon', noCache: true }
    }]
  },

  { path: '*', redirect: '/404', hidden: true }
]

export const simplifyasyncRouterMap = [
  {
    path: '/myevent',
    component: SimplifyLayout,
    redirect: '/myevent/list',
    name: 'event',
    meta: {
      title: 'Event',
      icon: 'event'
    },
    children: [
      { path: 'create', component: () => import('@/views/myevent/create'), name: 'createMyevent', meta: { title: 'Create', tagtitle: 'CreateEvent', icon: 'edit' }, hidden: true },
      { path: 'edit/:id(\\d+)', component: () => import('@/views/myevent/edit'), name: 'editMyevent', meta: { title: 'Edit', tagtitle: 'EditEvent', noCache: true }, hidden: true },
      { path: 'cost/:id(\\d+)', component: () => import('@/views/myevent/cost'), name: 'costMyevent', meta: { title: 'Cost', tagtitle: 'CostEvent', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/myevent/view'), name: 'viewEvent', meta: { title: 'View', tagtitle: 'ViewEvent', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/myevent/list'), name: 'listEvent', meta: { title: 'Events', tagtitle: 'Events', icon: 'event' }},
      { path: 'recent', component: () => import('@/views/myevent/listrecent'), name: 'listRecentEvent', meta: { title: 'List', tagtitle: 'ListEvent', icon: 'list' }, hidden: true }
    ]
  },
  {
    path: '/report',
    component: SimplifyLayout,
    redirect: '/report/list',
    children: [
      {
        path: 'list',
        component: () => import('@/views/report/list'),
        name: 'report',
        meta: { title: 'Report', tagtitle: 'ListReport', icon: 'documentation' }
      }
    ]
  },
  {
    path: '/contact',
    component: SimplifyLayout,
    redirect: '/contact/list',
    children: [
      {
        path: 'list',
        component: () => import('@/views/contact/list'),
        name: 'contact',
        meta: { title: 'Contact', tagtitle: 'ListContact', icon: 'wechat' }
      }
    ]
  },
  {
    path: '/hotel',
    component: SimplifyLayout,
    redirect: '/hotel/list',
    name: 'hotel',
    meta: {
      title: 'Hotel',
      icon: 'hotel'
    },
    children: [
      { path: 'create', component: () => import('@/views/hotel/create'), name: 'createHotel', meta: { title: 'Create', tagtitle: 'CreateHotel', icon: 'edit' }, hidden: true },
      { path: 'edit/:id(\\d+)', component: () => import('@/views/hotel/edit'), name: 'editHotel', meta: { title: 'Edit', tagtitle: 'EditHotel', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/hotel/view'), name: 'viewHotel', meta: { title: 'View', tagtitle: 'ViewHotel', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/hotel/list'), name: 'listHotel', meta: { title: 'Hotel', icon: 'hotel' }, hidden: false }
    ],
    hidden: true
  },
  {
    path: '/restaurant',
    component: SimplifyLayout,
    redirect: '/restaurant/list',
    name: 'restaurant',
    meta: {
      title: 'Restaurant',
      icon: 'restaurant'
    },
    children: [
      { path: 'create', component: () => import('@/views/restaurant/create'), name: 'createRestaurant', meta: { title: 'Create', tagtitle: 'CreateRestaurant', icon: 'edit' }, hidden: true },
      { path: 'edit/:id(\\d+)', component: () => import('@/views/restaurant/edit'), name: 'editRestaurant', meta: { title: 'Edit', tagtitle: 'EditRestaurant', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/restaurant/view'), name: 'viewRestaurant', meta: { title: 'View', tagtitle: 'ViewRestaurant', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/restaurant/list'), name: 'listRestaurant', meta: { title: 'Restaurant', icon: 'restaurant' }, hidden: false }
    ],
    hidden: true
  },
  {
    path: '/mycontact',
    component: SimplifyLayout,
    redirect: '/mycontact/list',
    name: 'mycontact',
    meta: {
      title: 'My Contacts',
      icon: 'event'
    },
    children: [
      { path: 'promoter/create', component: () => import('@/views/mycontact/promoter/create'), name: 'myContactCreatePromoter', meta: { title: 'Create', icon: 'edit' }, hidden: true },
      { path: 'promoter/edit/:id(\\d+)', component: () => import('@/views/mycontact/promoter/edit'), name: 'myContacEditPromoter', meta: { title: 'Edit', noCache: true }, hidden: true },
      { path: 'promoter/view/:id(\\d+)', component: () => import('@/views/mycontact/promoter/view'), name: 'myContactViewPromoter', meta: { title: 'View', noCache: true }, hidden: true },
      { path: 'promoter/list', component: () => import('@/views/mycontact/promoter/list'), name: 'myContactListPromoter', meta: { title: 'Promoter', icon: 'promoter' }},
      { path: 'production/create', component: () => import('@/views/mycontact/production/create'), name: 'myContactCreateProduction', meta: { title: 'Create', icon: 'edit' }, hidden: true },
      { path: 'production/edit/:id(\\d+)', component: () => import('@/views/mycontact/production/edit'), name: 'myContacEditProduction', meta: { title: 'Edit', noCache: true }, hidden: true },
      { path: 'production/view/:id(\\d+)', component: () => import('@/views/mycontact/production/view'), name: 'myContactViewProduction', meta: { title: 'View', noCache: true }, hidden: true },
      { path: 'production/list', component: () => import('@/views/mycontact/production/list'), name: 'myContactListProduction', meta: { title: 'Production', icon: 'production' }},
      { path: 'production/:id(\\d+)/staff', component: () => import('@/views/mycontact/production/staff'), name: 'myContactstaffProduction', meta: { title: 'List', tagtitle: 'ListStaff', icon: 'list' }, hidden: true },
      { path: 'manager/create', component: () => import('@/views/mycontact/manager/create'), name: 'myContactCreateManager', meta: { title: 'Create', icon: 'edit' }, hidden: true },
      { path: 'manager/edit/:id(\\d+)', component: () => import('@/views/mycontact/manager/edit'), name: 'myContacEditManager', meta: { title: 'Edit', noCache: true }, hidden: true },
      { path: 'manager/view/:id(\\d+)', component: () => import('@/views/mycontact/manager/view'), name: 'myContactViewManager', meta: { title: 'View', noCache: true }, hidden: true },
      { path: 'manager/list', component: () => import('@/views/mycontact/manager/list'), name: 'myContactListManager', meta: { title: 'Manager', icon: 'manager' }},
      { path: 'artist/create', component: () => import('@/views/mycontact/artist/create'), name: 'myContactCreateArtist', meta: { title: 'Create', icon: 'edit' }, hidden: true },
      { path: 'artist/edit/:id(\\d+)', component: () => import('@/views/mycontact/artist/edit'), name: 'myContacEditArtist', meta: { title: 'Edit', noCache: true }, hidden: true },
      { path: 'artist/view/:id(\\d+)', component: () => import('@/views/mycontact/artist/view'), name: 'myContactViewArtist', meta: { title: 'View', noCache: true }, hidden: true },
      { path: 'artist/list', component: () => import('@/views/mycontact/artist/list'), name: 'myContactListArtist', meta: { title: 'Artist', icon: 'artist' }},
      { path: 'technician/create', component: () => import('@/views/mycontact/technician/create'), name: 'myContactCreateTechnician', meta: { title: 'Create', icon: 'edit' }, hidden: true },
      { path: 'technician/edit/:id(\\d+)', component: () => import('@/views/mycontact/technician/edit'), name: 'myContacEditTechnician', meta: { title: 'Edit', noCache: true }, hidden: true },
      { path: 'technician/view/:id(\\d+)', component: () => import('@/views/mycontact/technician/view'), name: 'myContactViewTechnician', meta: { title: 'View', noCache: true }, hidden: true },
      { path: 'technician/list', component: () => import('@/views/mycontact/technician/list'), name: 'myContactListTechnician', meta: { title: 'Technician', icon: 'technician' }}
    ],
    hidden: true
  },
  {
    path: '/user',
    component: SimplifyLayout,
    redirect: '/user/list',
    name: 'user',
    meta: {
      title: 'User',
      icon: 'user'
    },
    hidden: true,
    children: [
      { path: 'edit/:id(\\d+)', component: () => import('@/views/user/edit'), name: 'editUser', meta: { title: 'Edit', tagtitle: 'EditUser', noCache: true }, hidden: true },
      { path: 'view/:id(\\d+)', component: () => import('@/views/user/view'), name: 'viewUser', meta: { title: 'View', tagtitle: 'ViewUser', noCache: true }, hidden: true },
      { path: 'list', component: () => import('@/views/user/list'), name: 'userList', meta: { title: 'List', icon: 'list', tagtitle: 'ListUser', noCache: true }, hidden: true }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]
